<div align="center">

  <img src="assets/logo.svg" alt="logo" width="200" height="auto" />
  <h1>crackegg</h1>
  
  <p>
    confirm own gender
  </p>
  
  
<!-- Badges -->
  <p>
    <a href="https://gitlab.com/TheOPtimal/crackegg/-/graphs/main">
      <img src="https://img.shields.io/gitlab/contributors/TheOPtimal/crackegg?style=flat-square" alt="contributors" />
    </a>
    <a href="https://gitlab.com/TheOPtimal/crackegg/-/commits/main">
      <img src="https://img.shields.io/gitlab/last-commit/TheOPtimal/crackegg?style=flat-square" alt="last update" />
    </a>
    <a href="https://gitlab.com/TheOPtimal/crackegg/-/forks">
      <img src="https://img.shields.io/gitlab/forks/TheOPtimal/crackegg?style=flat-square" alt="forks" />
    </a>
    <a href="https://gitlab.com/TheOPtimal/crackegg/-/starrers">
      <img src="https://img.shields.io/gitlab/stars/TheOPtimal/crackegg?style=flat-square" alt="stars" />
    </a>
    <a href="https://gitlab.com/TheOPtimal/crackegg/-/issues">
      <img src="https://img.shields.io/gitlab/issues/open-raw/TheOPtimal/crackegg?style=flat-square" alt="open issues" />
    </a>
    <a href="https://gitlab.com/TheOPtimal/crackegg/-/blob/main/LICENSE.md">
      <img src="https://img.shields.io/gitlab/license/TheOPtimal/crackegg?style=flat-square" alt="license" />
    </a>
  </p>
   
  <h4>
      <a href="https://gitlab.com/TheOPtimal/crackegg/-/issues">Report Bug</a>
    <span> · </span>
      <a href="https://gitlab.com/TheOPtimal/crackegg/-/issues">Request Feature</a>
  </h4>
</div>

<br />


## 🌟 About the Project

### 📷 Screenshots

![Screenshot](assets/screenshot.png)

### 👾 Stack

- [Nix](https://nixos.org)
- [Rust](https://rust-lang.org)
- [Clap](https://docs.rs/clap)

## 🧰 Getting Started

### ‼️ Prerequisites

This project uses [Nix](https://nixos.org) as build & testing tool. You will also need [flakes](https://nixos.wiki/wiki/Flakes) enabled.

### ⚡ Quick Start

If you want to avoid building the package yourself, you can import the CI artifact into your store:

```bash
curl -sSfL "https://gitlab.com/TheOPtimal/crackegg/-/jobs/artifacts/main/raw/crackegg.nar?job=build" -o - | nix-store --import
```

Quickly get into a shell with the program:

```bash
  nix shell gitlab:TheOPtimal/crackegg
```

Alternatively, run it directly from your current shell:

```bash
  nix run gitlab:TheOPtimal/crackegg -- --help
```

### ⬇️ Cloning

```bash
  git clone https://gitlab.com/TheOPtimal/crackegg.git
  cd crackegg
```

### ⚙️ Initialization

Get into the project's development environment:

```bash
  nix develop
```

Alternatively, use direnv:

```bash
  direnv allow
```
   
### 🧪 Running Tests

To run tests, run the following command:

```bash
  nix flake check
```

Tests are also automatically run before a push happens.

### 🎽 Running

Use nix shell to enter a shell with the program:

```bash
  nix shell .
```

Alernatively, run it directly from the current shell:

```bash
  nix run . -- --help
```

Or use cargo (recommended during development):

```bash
  cargo run -- --help
```

## 👀 Usage

```
convince of own gender

Usage: crackegg [OPTIONS] <GENDER>

Arguments:
  <GENDER>
          Gender to confirm. If you don't see yours here, please add it

          Possible values:
          - man:         Man <hr style="border-top:0px;border-style:dashed;">
          - woman:       Woman <hr style="border-top:0px;border-style:dashed;">
          - non-binary:  Non Binary <hr style="border-top:0px;border-style:dashed;">
          - intersex:    Intersex <hr style="border-top:0px;border-style:dashed;">
          - agender:     Agender <hr style="border-top:0px;border-style:dashed;">
          - bigender:    Bigender <hr style="border-top:0px;border-style:dashed;">
          - genderfluid: Genderfluid <hr style="border-top:0px;border-style:dashed;">

Options:
  -f, --force
          Work as fast as possible, allowing "rough handling"; this option may only be used by the superuser

  -n, --name <NAME>
          Name of the user. If provided, additional name affirmation will be provided aswell

  -h, --help
          Print help information (use `-h` for a summary)

  -V, --version
          Print version information

```

## 👋 Contributing

Contributions are always welcome!

Especially, help is wanted in these areas:

- ⚧️ Adding more genders
- ❤️ More affirmations for existing genders
- 📦 Packaging for different distros

### 📜 Code of Conduct

Please read the [Code of Conduct](https://gitlab.com/TheOPtimal/crackegg/-/blob/main/CODE_OF_CONDUCT.md)

## ⚠️ License

Distributed under the GPLv3-or-later license. See [LICENSE.md](https://gitlab.com/TheOPtimal/crackegg/-/blob/main/LICENSE.md) for more information.

## 💎 Acknowledgements

- [Shields.io](https://shields.io/)
- [Awesome README](https://github.com/matiassingers/awesome-readme)
- [Athena-Lilith Martin](https://web.archive.org/web/20230528215711/https://solarpunk.moe/@alilly/109272240352109427)
