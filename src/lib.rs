// crackegg - convince of own gender
// Copyright (C) 2022  Jacob Gogichishvili

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod affirmations;
pub mod cli;
pub mod gender;

use affirmations::Affirmations;
pub use cli::Args;
pub use gender::Gender;

use nix::unistd::Uid;
use rand::seq::IteratorRandom;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Tried to use force without being superuser")]
    ForceNotRoot,

    #[error("No affirmations for this gender. If you can, please add it.")]
    MissingAffirmations,
}

/// Turn static const array into a mutable string vector.
fn transform(list: &'static [&'static str]) -> Vec<String> {
    list.iter().map(|&str| str.to_string()).collect()
}

/// Get proper affirmations for the user's gender, name, and force preference.
fn get_affirm(args: &cli::Args) -> Result<Vec<String>, Error> {
    let Affirmations {
        affirmations,
        name_affirmations,
        rough_affirmations,
    } = args.gender.value();

    if args.force {
        // if user tries to use --force, check if they're root. if not, return error.
        if !Uid::effective().is_root() {
            Err(Error::ForceNotRoot)
        } else {
            Ok(transform(rough_affirmations))
        }
    } else if let Some(name) = &args.name {
        // replace all instances of {} with user's name
        Ok(name_affirmations
            .iter()
            .map(|str| str.replace("{}", name))
            .chain(transform(affirmations))
            .collect())
    } else {
        Ok(transform(affirmations))
    }
}

pub fn app(args: cli::Args) -> Result<(), Error> {
    let mut rng = rand::thread_rng();
    let affirmations = get_affirm(&args)?;

    // choose random affirmation string
    let confirmation = affirmations
        .iter()
        .choose(&mut rng)
        .ok_or(Error::MissingAffirmations)?;

    println!("{confirmation}");

    Ok(())
}
