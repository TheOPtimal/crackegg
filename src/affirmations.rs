//! Affirmations for each gender
//!
//! This module provides affirmations for available genders. It's seperated into a file for easy
//! editing.
//!
//! # Notes
//!
//! If an additional gender is added, it also needs to be specified in `gender.rs`
//!
//! `name_affirmations` calls the user by their preffered chosen name, anti-deadnaming them.

// crackegg - convince of own gender
// Copyright (C) 2022  Jacob Gogichishvili

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

type ListRef = &'static [&'static str];

/// Creates an affirmation for a specific gender
///
/// For `name_affirmations`, `{}` is replaced with the user's name.
///
/// # Examples
///
/// ```
/// use crackegg::affirmations::Affirmations;
///
/// pub const EXAMPLE: Affirmations = Affirmations {
///     affirmations: &["you are valid and are of example gender."],
///     name_affirmations: &["{} is a beautiful name, and it's YOUR name!", "hey {}, you are an amazing person of example gender."],
///     rough_affirmations: &["i have no idea what a rough affirmation would be"],
/// };
/// ```
pub struct Affirmations {
    pub affirmations: ListRef,
    pub name_affirmations: ListRef,
    pub rough_affirmations: ListRef,
}

pub const MAN: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};

pub const WOMAN: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};

pub const NON_BINARY: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};

pub const INTERSEX: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};

pub const AGENDER: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};

pub const BIGENDER: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};

pub const GENDERFLUID: Affirmations = Affirmations {
    affirmations: &[],
    name_affirmations: &[],
    rough_affirmations: &[],
};
