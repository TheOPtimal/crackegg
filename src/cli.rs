// crackegg - convince of own gender
// Copyright (C) 2022  Jacob Gogichishvili

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::gender::Gender;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    /// Work as fast as possible, allowing "rough handling"; this option may only be used by the
    /// superuser.
    #[arg(short, long)]
    pub force: bool,

    /// Name of the user. If provided, additional name affirmation will be provided aswell.
    #[arg(short, long, value_name = "NAME")]
    pub name: Option<String>,

    /// Gender to confirm. If you don't see yours here, please add it.
    #[arg(value_enum, value_name = "GENDER")]
    pub gender: Gender,
}
