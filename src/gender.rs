// crackegg - convince of own gender
// Copyright (C) 2022  Jacob Gogichishvili

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::affirmations::*;
use clap::ValueEnum;
use vnum::value_enum;

value_enum! {
    /// Defines genders and their affirmations/confirmations
    #[derive(Clone, PartialEq, Eq, Debug, ValueEnum)]
    pub enum Gender: Affirmations {
        /// Man
        Man = MAN,
        /// Woman
        Woman = WOMAN,
        /// Non Binary
        NonBinary = NON_BINARY,
        /// Intersex
        Intersex = INTERSEX,
        /// Agender
        Agender = AGENDER,
        /// Bigender
        Bigender = BIGENDER,
        /// Genderfluid
        Genderfluid = GENDERFLUID,
    }
}
