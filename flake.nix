# crackegg - convince of own gender
# Copyright (C) 2022  Jacob Gogichishvili

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

{
  description = "Convince of own gender";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, crane, flake-utils, advisory-db, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        inherit (pkgs) lib;

        craneLib = crane.lib.${system};
        src = craneLib.cleanCargoSource ./.;

        # Build *just* the cargo dependencies, so we can reuse
        # all of that work (e.g. via cachix) when running in CI
        cargoArtifacts = craneLib.buildDepsOnly {
          inherit src;
        };

        # Build the actual crate itself, reusing the dependency
        # artifacts from above.
        crackegg = craneLib.buildPackage {
          inherit cargoArtifacts src;

          outputs = ["out" "man"];

          # Install man page and shell completions
          postInstall = ''
            # Copy man files to their proper locations
            mkdir -p $out/share/man/man1
            cp target/release/build/$pname-*/out/$pname.1 $out/share/man/man1


            # Copy shell completion files to proper locations
            mkdir -p $out/share/bash-completion/completions
            cp target/release/build/$pname-*/out/$pname.bash $out/share/bash-completion/completions/$pname

            mkdir -p $out/share/fish/vendor_completions.d
            cp target/release/build/$pname-*/out/$pname.fish $out/share/fish/vendor_completions.d

            mkdir -p $out/share/zsh/site-functions
            cp target/release/build/$pname-*/out/_$pname $out/share/zsh/site-functions
          '';
        };
      in
      {
        checks = {
          # Build the crate as part of `nix flake check` for convenience
          inherit crackegg;

          # Run clippy (and deny all warnings) on the crate source,
          # again, resuing the dependency artifacts from above.
          #
          # Note that this is done as a separate derivation so that
          # we can block the CI if there are issues here, but not
          # prevent downstream consumers from building our crate by itself.
          crackegg-clippy = craneLib.cargoClippy {
            inherit cargoArtifacts src;
            cargoClippyExtraArgs = "--all-targets -- --deny warnings";
          };

          crackegg-doc = craneLib.cargoDoc {
            inherit cargoArtifacts src;
          };

          # Check formatting
          crackegg-fmt = craneLib.cargoFmt {
            inherit src;
          };

          # Audit dependencies
          crackegg-audit = craneLib.cargoAudit {
            inherit src advisory-db;
          };

          # Run tests with cargo-nextest
          # Consider setting `doCheck = false` on `crackegg` if you do not want
          # the tests to run twice
          crackegg-nextest = craneLib.cargoNextest {
            inherit cargoArtifacts src;
            partitions = 1;
            partitionType = "count";
          };
        } // lib.optionalAttrs (system == "x86_64-linux") {
          # NB: cargo-tarpaulin only supports x86_64 systems
          # Check code coverage (note: this will not upload coverage anywhere)
          crackegg-coverage = craneLib.cargoTarpaulin {
            inherit cargoArtifacts src;
          };
        };

        packages.default = crackegg;

        apps.default = flake-utils.lib.mkApp {
          drv = crackegg;
        };

        devShells.default = pkgs.mkShell {
          inputsFrom = builtins.attrValues self.checks;

          # Extra inputs can be added here
          packages = with pkgs; [
            cargo
            rustc
            evcxr
          ];

          shellHook = ''
            git config --local core.hooksPath $PWD/.githooks
          '';
        };
      });
}
