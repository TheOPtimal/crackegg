// crackegg - convince of own gender
// Copyright (C) 2022  Jacob Gogichishvili

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use clap::CommandFactory;
use clap_complete::{generate_to, shells::*};

#[path = "src/affirmations.rs"]
mod affirmations;
#[path = "src/cli.rs"]
mod cli;
#[path = "src/gender.rs"]
mod gender;

fn main() -> std::io::Result<()> {
    // Build directory
    let out_dir =
        std::path::PathBuf::from(std::env::var_os("OUT_DIR").ok_or(std::io::ErrorKind::NotFound)?);

    // Command & arguments
    let mut cmd = cli::Args::command();

    // Render man page
    let man = clap_mangen::Man::new(cmd.clone());
    let mut buffer: Vec<u8> = Default::default();
    man.render(&mut buffer)?;

    let render_path = out_dir.join("crackegg.1");
    std::fs::write(&render_path, buffer)?;

    println!("cargo:warning=man page is rendered: {:?}", render_path);

    // Generate completions

    // Bash completions
    let bash_path = generate_to(
        Bash, &mut cmd,   // We need to specify what generator to use
        "crackegg", // We need to specify the bin name manually
        &out_dir,   // We need to specify where to write to
    )?;

    println!(
        "cargo:warning=bash completion file is generated: {:?}",
        bash_path
    );

    // Zsh completions
    let zsh_path = generate_to(
        Zsh, &mut cmd,   // We need to specify what generator to use
        "crackegg", // We need to specify the bin name manually
        &out_dir,   // We need to specify where to write to
    )?;

    println!(
        "cargo:warning=zsh completion file is generated: {:?}",
        zsh_path
    );

    // Fish completions
    let fish_path = generate_to(
        Fish, &mut cmd,   // We need to specify what generator to use
        "crackegg", // We need to specify the bin name manually
        &out_dir,   // We need to specify where to write to
    )?;

    println!(
        "cargo:warning=fish completion file is generated: {:?}",
        fish_path
    );

    Ok(())
}
